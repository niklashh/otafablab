- name: Install dependencies
  tags: slow
  become: yes
  apt:
    name:
    - acl
    - python3-pip
    - python3-dev
    - python3-setuptools
    - python3-virtualenv
    - git
    - libyaml-dev
    - build-essential
    - ffmpeg

- name: Add octoprint user
  become: yes
  user:
    name: octoprint
    uid: 1001
    groups: dialout
    append: yes
    shell: /bin/bash
    create_home: yes
    home: /opt/octoprint

- name: Install and enable Octoprint
  become: yes
  become_user: octoprint
  block:
  - name: Create virtualenv
    tags: slow
    command: virtualenv venv
    args:
      chdir: /opt/octoprint
      creates: /opt/octoprint/venv

  - name: Install newest Octoprint
    tags: slow
    shell: >-
      cd /opt/octoprint
      &&  source venv/bin/activate
      && pip install pip --upgrade
      && pip install https://get.octoprint.org/latest
    args:
      executable: /bin/bash

  - name: Create .octoprint
    file:
      state: directory
      path: ~/.octoprint

  - name: Create Octoprint systemd service
    become_user: root
    template:
      src: octoprint.service.j2
      dest: /etc/systemd/system/octoprint.service
      owner: root
      group: root
      mode: 0644

  - name: Enable Octoprint service
    become_user: root
    systemd:
      name: octoprint.service
      enabled: yes
      state: started

- name: Configure webcam
  import_tasks: webcam.yml
  tags: webcam

- name: Configure Octoprint
  become: yes
  become_user: octoprint
  block:
  - name: Wait for Octoprint to start
    wait_for:
      path: ~/.octoprint/config.yaml
      search_regex: '^api:'

  - name: Read config.yaml
    slurp:
      path: ~/.octoprint/config.yaml
    register: _config_yaml

  - name: Set config_yaml
    set_fact:
      config_yaml: "{{ _config_yaml['content'] | b64decode | from_yaml }}"

  - name: Wait until server is started
    uri:
      url: http://{{ ansible_host }}:5000/api/server
      headers:
        X-Api-Key: "{{ config_yaml.api.key }}"
    register: _result
    until: _result.status == 200
    retries: 30
    delay: 1

  - name: Stop Octoprint service
    become_user: root
    systemd:
      name: octoprint.service
      state: stopped

  - name: Read config.yaml
    slurp:
      path: ~/.octoprint/config.yaml
    register: _config_yaml

  - name: Set config_yaml
    set_fact:
      config_yaml: "{{ _config_yaml['content'] | b64decode | from_yaml }}"

  - name: Update config_yaml
    ansible.utils.update_fact:
      updates:
        - path: config_yaml.accessControl
          value:
            salt: "{{ lookup('community.general.random_string', special = False, length = 32) }}"
            autologinLocal: true
            autologinAs: admin
            localNetworks:
              - 0.0.0.0/0
        - path: config_yaml.api.allowCrossOrigin
          value: true
        - path: config_yaml.api.key
          value: "00000000000000000000000000000000"
        - path: config_yaml.server.host
          value: "{{ wireguard_address }}"
        - path: config_yaml.server.onlineCheck
          value:
            enabled: false
        - path: config_yaml.plugins.tracking
          value:
            enabled: false
        - path: config_yaml.server.pluginBlacklist
          value:
            enabled: false
        - path: config_yaml.server.firstRun
          value: false
        - path: config_yaml.server.cookies
          value:
            secure: true
        - path: config_yaml.server.ipCheck
          value:
            enabled: false
        - path: config_yaml.printerProfiles.default
          value: ultimaker_2
        - path: config_yaml.webcam
          value:
            ffmpeg: /usr/bin/ffmpeg
            flipH: true
            flipV: true
            snapshot: "http://fablab.rip/{{ inventory_hostname }}/stream/?action=snapshot"
            stream: "https://fablab.rip/{{ inventory_hostname }}/stream/?action=stream"
            streamRatio: '4:3'
            watermark: false
    register: updated

  - name: Copy config_yaml
    copy:
      content: "{{ updated.config_yaml | to_yaml }}"
      dest: ~/.octoprint/config.yaml
      owner: octoprint
      group: octoprint

  - name: Copy users.yaml
    template:
      src: users.yaml.j2
      dest: ~/.octoprint/users.yaml
      owner: octoprint
      group: octoprint
    vars:
      password: kissa1234
      salt: "{{ updated.config_yaml.accessControl.salt }}"

  - name: Copy printer profiles
    template:
      src: "{{ item }}.profile.j2"
      dest: ~/.octoprint/printerProfiles/{{ item }}.profile
      owner: octoprint
      group: octoprint
    loop:
      - ultimaker_2
      - prenta_duo_xl

  - name: Start Octoprint service
    become_user: root
    systemd:
      daemon_reload: yes
      name: octoprint.service
      state: started

  - name: Wait until server is started
    uri:
      url: http://{{ ansible_host }}:5000/api/server
      headers:
        X-Api-Key: "{{ updated.config_yaml.api.key }}"
    register: _result
    until: _result.status == 200
    retries: 30
    delay: 1
