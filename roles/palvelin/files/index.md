---
title: Otaniemen lukion fablab
---

::: Navbar

<div class="logo">
![Otakarhu](Otakarhu.png)
</div>

# OTANIEMEN LUKION FABLAB

- [Tervetuloa](#tervetuloa-fablabiin)
- [3d-Mallintaminen](#d-mallintaminen)
- [Yhteystiedot](#yhteystiedot)

:::

::: Content
## Tervetuloa Fablabiin

### Telegram

[Liity nyt Fablabin telegram ryhmään!](https://t.me/joinchat/WYaf48cddcMzZjU8)

### Säännöt

Fablabin säännöt

1. Vie kaikki roskat roskiin
2. Älä käytä useaa tulostinta samaan aikaan
3. Keskeytä tulostus joka epäonnistuu

## 3D-Mallintaminen

### Configurations

- [Ultimaker 2+](https://gitlab.com/otafablab/prusaslicer-config/-/raw/main/ultimaker2+.ini?inline=false)
- [Prenta Duo XL](https://gitlab.com/otafablab/prusaslicer-config/-/raw/main/prenta_duo_xl.ini?inline=false)

### 3D-mallintaminen ja suuunnittelu - lukiokurssi

- [Osa 1](https://www.lukemaverkosto.fi/materiaali/3d-mallintaminen-ja-tulostaminen-lukiokurssi-osa-1/)
- [Osa 2](https://www.lukemaverkosto.fi/materiaali/3d-mallintaminen-ja-tulostaminen-lukiokurssi-osa-2/)
- [Osa 3](https://www.lukemaverkosto.fi/materiaali/3d-mallintaminen-ja-tulostaminen-lukiokurssi-osa-3/)
- [Osa 4](https://www.lukemaverkosto.fi/materiaali/3d-mallintaminen-ja-tulostaminen-lukiokurssi-osa-4/)
- [Osa 5](https://www.lukemaverkosto.fi/materiaali/3d-mallintaminen-ja-tulostaminen-lukiokurssi-osa-5/)

## TEC11 Rust-kurssi

![Eeppinen
mainoskuva](https://gitlab.com/otafablab/rust-lukiokurssi/-/raw/cea5574e1d484989ff996a4eaf4f1155d5bbd416/rustkurssi.png)

Tervetuloa Rust-ohjelmoinnin kurssille 2. periodissa syksyllä 2022!

[Rust](https://rust-lang.org) on [Mozilla
Foundationin](https://foundation.mozilla.org) kehittämä staattisesti
sekä dynaamisesti tyypi tetty olio-orientoitu imperatiivinen
funktionaalinen ekspressiivinen korkean tason turvallinen
järjestelmäohjelmointikieli, joka on avointa lähdekoodia.

Kurssi on tarkoitettu ohjelmointiin jo tutustuneille oppilaille.

> Esimerkkejä ohjelmointikielistä, joiden osaamisesta on hyötyä kurssilla:
> - Python
> - JavaScript
> - Java
> - Scala
> - C/C++

Jos olet käynyt Aallon Python-kurssin tai Koodi101-kurssin, kurssi on
sinulle sopivan tasoinen.

Tällä kurssilla opit matalan tason ohjelmoinnin pääkonseptit, kuten
pino, keko ja pointterit. Kurssilla opitaan myös korkean tason
ohjelmoinnin konsepteja, kuten abstraktiot, tyypit, omistajuus ja
korkeamman asteen funktiot. Opit myös yleistä ATK tietoa, kuten:
suoritin, muisti, prosessi, kääntäjä, testaus, optimointi.

Kurssilla on viikottain kaksi oppituntia, joista toinen on tarkoitettu
luennolle ja toinen tehtävien tekemistä varten. Kurssilla on kuitenkin
paljon tehtäviä, joita pitää ratkaista omalla ajalla. Kurssin voi
suorittaa myös osittain itsenäisesti.

Kurssilla on käytössä ennennäkemätön
VSCode-kehitysympäristöjärjestelmä ja kurssia varten kehitetty
[otarustlings-tehtäväalustajärjestelmä](https://otafablab.gitlab.io/rust-lukiokurssi/otarustlings.html).

Jos haluat tutustua kurssimateriaaliin etukäteen, löydät sen
[täältä](https://otafablab.gitlab.io/rust-lukiokurssi).

## Sivuston lähdekoodi

[Gitlab organisaatio](https://gitlab.com/otafablab/)

## Yhteystiedot

### Ylläpidon yhteystiedot

- Niklas Halonen, Fablab corp toimitusjohtaja
  - <https://t.me/niklashh>
- Matias Zwinger, Fablab corp varapuheenjohtaja ja laboratoriomanageri
  - <https://t.me/matias_z>
- Matti Heikkinen, Matematiikan ja fysiikan opettaja
  - <https://t.me/Heikkinen>
- Severi Lybeck, FabLab-Verkkosivun teema
:::
